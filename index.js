import React from 'react';
import ReactDOM from 'react-dom';
import App from './src/App';
import Reducer from './src/reducer'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import {createLogger} from 'redux-logger'
import Routes from './src/routes'

const store = createStore(Reducer,applyMiddleware(createLogger({collapsed:true})))

ReactDOM.render(
    <Provider store={store}>
        <Routes />
    </Provider>, document.getElementById('app'))
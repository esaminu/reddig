# [Reddigg](https://reddigg.herokuapp.com/)

[Reddigg](https://reddigg.herokuapp.com/) is a reddit/digg style application built with react, redux and react-router. Its deployed on Heroku at reddigg.herokuapp.com

## How to run
First, clone the repo and install dependencies

```sh
$ git clone https://esaminu@bitbucket.org/esaminu/reddig.git
$ cd reddig
$ yarn install
```

After that you can either run the development server or build for prodcution.

### Development Server

To run the dev server just do `yarn run dev`. You can now access the app at `localhost:8080`

### Production bundle and server

To create the production bundle do `yarn run build`. you should now see the `bundle.js` on the project root.

After that you can run `yarn run prod` and access the app at `localhost:8080`
import React, {Component} from 'react';
import styles from '../styles/navBar.css'

class NavBar extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            toggleMenu: false
        }
    }

    render() {
        return (
            <div className="nav">
                <div className="homecont">
                    <a  onClick={()=>this.props.history.push('/')} className="home">Reddigg</a>
                    <div style={{paddingRight:10}} className="menubtncont" onClick={()=>this.setState({toggleMenu:!this.state.toggleMenu})}>
                        <div className="menubtn"></div>
                        <div className="menubtn"></div>
                        <div className="menubtn"></div>
                    </div>
                </div>
                {this.state.toggleMenu || this.props.width >= 700 ?
                    <div className="navMenu">
                        <a onClick={()=>this.props.history.push('/posts/new')} className="btn">new</a>
                        <div className="btn top">
                            top &#9662;
                            <div className="dropdown">
                                <a onClick={()=>this.props.history.push('/posts/topday')} className="item">Today</a>
                                <a onClick={()=>this.props.history.push('/posts/topweek')} className="item">Last Week</a>
                                <a onClick={()=>this.props.history.push('/posts/topall')} className="item">All Time</a>
                            </div>
                        </div>
                        <a onClick={()=>this.props.history.push('/about')} className="btn">about</a>
                    </div> : undefined
                }
            </div>
        )
    }
} 

export default NavBar
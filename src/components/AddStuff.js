import React, {Component} from 'react';
import styles from '../styles/mainPage.css'
import uuidv1 from 'uuid'
import AddBtn from './mobileAddBtn'

class AddStuff extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            addState: false,
            textLength: 0,
            text: ''
        }
    }

    onChangeText(text) {
        text.length <= 255 ?
        this.setState({text,textLength:text.length}) :
        this.setState({text:text.substring(0,255),textLength:255})
    }

    render(){
        return (
            <div className="addbtns">
                {this.props.width <= 700 ? 
                    <AddBtn addState={this.state.addState} 
                    onChangeText={text=>this.onChangeText(text)}
                    onBlurPopup={e => !e.currentTarget.contains(e.relatedTarget) ? this.setState({addState: false}) : undefined}
                    onClickBtn={()=>this.setState({addState: true})} text={this.state.text} textLength={this.state.textLength}
                    add={this.props.addTopic} /> : 
                    <div style={{margin:20}}>
                       <span style={{fontFamily:'Arial',color:'#e53935'}}>Add Post <br/> </span>
                       <textarea style={{height:60}} className="postText" value={this.state.text} 
                        onChange={text=> this.onChangeText(text.target.value)} 
                        onKeyDown={e=> {
                            if(e.key === 'Enter') {
                                e.preventDefault()
                                if(this.state.textLength > 0){
                                    this.props.addTopic({votes:0,text:this.state.text,timestamp: new Date().toISOString(),id:uuidv1()})
                                    this.onChangeText('')
                                }
                            } 
                        }}/> 
                        <div className="popupFooter">
                            <span style={{fontSize:16,paddingRight:20}}>{this.state.textLength + ' / 255'}</span>
                            <button className="small" onClick={()=>{
                                if(this.state.textLength > 0) {
                                    this.props.addTopic({votes:0,text:this.state.text,timestamp: new Date().toISOString(),id:uuidv1()})
                                    this.onChangeText('')
                                }
                            }}>Add &#9547;</button>
                        </div> 
                    </div>
                }
                <div className="addbtn addtest" onClick={()=>this.props.addTest()}>Add test data</div>
            </div>
        )
    }
}

export default AddStuff
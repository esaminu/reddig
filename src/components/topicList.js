import React, {Component} from 'react'
import Topic from './topic'
import {connect} from 'react-redux'
import styles from '../styles/mainPage.css'
import { TopX, getLatest, getLastWeekPosts, getTodayPosts, sortByVotes } from '../upvotesSorts'

const getTimeAgo = date => {
    var msdiff = Math.abs(new Date(date) - new Date())
    var s =  Math.floor((msdiff/1000)/60)
    var hrs = Math.round(s/60)
    var days = Math.round(s/1440)
    var months = Math.round(s/43200)
    switch(true) {
        case (s<=1): return 'less than a minute ago';
        case (s>60 && s<=1440): return hrs > 1 ? hrs + ' hours ago' : hrs + ' hour ago';
        case (s>1440 && s<=43200): return days > 1 ? days + ' days ago' : days + ' day ago';
        case (s>43200): return months > 1 ? months + ' months ago' : months + ' month ago';
        default: return s + ' minutes ago'
    }
}

const VisibleTopics = (topics,show) => {
    switch(show) {
        case 'new': return getLatest(topics)
        case 'topweek': return getLastWeekPosts(topics, new Date().toISOString())
        case 'topday': return getTodayPosts(topics,new Date().toISOString())
        case 'topall': return sortByVotes(topics,new Date().toISOString())
        default: return TopX(topics,20)
    }
}

export default 
connect((state,ownProps) => ({topics: VisibleTopics(state.topics,ownProps.show)}),
    dispatch => ({
        upvote: id => dispatch({type:'UPVOTE',id}),
        downvote: id => dispatch({type:'DOWNVOTE',id})
    })
)(
    ({topics,upvote,downvote}) => (
        <div className="topics">
            {topics.map((topic,idx) => 
            <Topic key={idx} votes={topic.votes} topic={topic.text} 
                upvote={()=>upvote(topic.id)} downvote={()=>downvote(topic.id)}
                timeAgo={getTimeAgo(topic.timestamp)} /> )}
        </div>
    )
)
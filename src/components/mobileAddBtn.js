import React, {Component} from 'react';
import styles from '../styles/mainPage.css'
import uuidv1 from 'uuid'

const AddBtn = ({addState,onChangeText,onBlurPopup,onClickBtn,text,textLength,add}) => (
    <div className="addbtn" tabIndex="0" 
        onBlur={e=>onBlurPopup(e)} 
        onClick={()=>onClickBtn()}>
        Add post
        {addState ?
            <div className="popup">
        ​       <textarea className="postText" value={text} 
                        onChange={text=> onChangeText(text.target.value)}
                        onKeyDown={e=>{
                            if(e.key === 'Enter') {
                                e.preventDefault()
                                if(textLength > 0){
                                    add({votes:0,text,timestamp: new Date().toISOString(),id:uuidv1()})
                                    onChangeText('')
                                }
                            }
                        }}/> 
                <div className="popupFooter">
                    <span style={{fontSize:16,paddingRight:20}}>{textLength + ' / 255'}</span>
                    <button className="small" onClick={()=>{
                        if(textLength > 0){
                            add({votes:0,text,timestamp: new Date().toISOString(),id:uuidv1()});
                            onChangeText('')
                        }
                    }}>Add &#9547;</button>
                </div>                           
            </div>
        : undefined} 
    </div>
)

export default AddBtn
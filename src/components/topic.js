import React, {Component} from 'react';
import styles from '../styles/topic.css';

const Topic = ({votes, topic, upvote, downvote,timeAgo}) => (
    <div className="topic">
        <div className="vote">
            <button className="votebtn" onClick={()=>upvote()}>&#9650;</button>
            <span>{votes}</span>
            <button className="votebtn" onClick={()=>downvote()}>&#9660;</button>
        </div>
        <div className="topicbody">
            <span>{topic}</span>
            <span style={{fontSize:10,marginTop:'5px',alignSelf:'flex-end'}}>{timeAgo}</span>
        </div>
    </div>
)

export default Topic;
import React,{Component} from 'react'
import { Route, HashRouter, Switch } from 'react-router-dom'
import App from './App'

const NotFound = () => <h1>404</h1>

const possibleShows = ['new','topweek','topday','topall']

const getTitle = show => {
    switch(show) {
        case 'new': return 'New'
        case 'topweek': return 'Top this Week'
        case 'topday': return 'Top Today'
        case 'topall': return 'Top All Time'
    }
}

const Routes = ({topics}) => {
    return (
        <HashRouter>
            <div>
                <Switch>
                    <Route exact path='/' render={props=><App title="Top 20" {...props} />} />
                    <Route exact path='/posts/:show' render={props=>possibleShows.includes(props.match.params.show) ? <App title={getTitle(props.match.params.show)} {...props}/> : <NotFound />} />
                    <Route exact path='/about' render={props=><App {...props}/>} />
                    <Route component={NotFound} />
                </Switch>
            </div>
        </HashRouter>
    )
}

export default Routes;
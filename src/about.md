# About This Page

This page will outline the key features of this app and go into how it was
out together including how the data is stored and other sorting/slicing operations that power it's functionality

## Reddigg Features

The key features of this app are: 

* Display top topics for the day
* Display top topics for the week
* Display latest topics
* Display top 20 topics
* Upvote and downvote a topic

## Tech used

* ReactJS
* Redux
* react-router

## Let's jump right in!

Reddig uses a JS array to store topics.
Each topic is an object with 4 keys: id, text, votes and timestamp 
where the timestamp is an ISO string and id is uuidv1. 
Better said in code, here's an example of a topic

```js
{
  "id":"b181bd69-1d3a-4097-aa26-70d70bee0563",
  "text":"donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit",
  "votes":-21,
  "timestamp":"2017-02-07T09:09:00Z"
}
```

The redux store has just one key: `{topics: []}`

Here, we'll only cover stuff performed directly on the array:

* Upvote and downvote
* Top topics for time period
* Latest topics
* Top 20

Out of the above, the only redux action dispatched is for upvote and downvote. In fact, for there are
only 4 redux actions for the whole app. Here's what the reducer looks like:

```js
export const Reducer = (state = {topics: []}, action) => {
    switch(action.type) {
        case 'ADD_TOPIC': return {...state, topics: state.topics.concat(action.topic)}
        case 'ADD_TEST_DATA': return {topics: action.topicData}
        case 'UPVOTE': return {topics: upvote(state.topics,action.id)}
        case 'DOWNVOTE': return {topics: downvote(state.topics,action.id)}
        default: return state
    }
}
```

The rest are handled in `mapStateToProps` by `VisibleTopics` and depend on the show param passed by react router ( `/posts/:show` )
the exported component looks like this: 
```js
    export default connect((state,ownProps) => ({
    	topics: VisibleTopics(state.topics,ownProps.show)
    })(theComponent)
```

and here's what `VisibleTopics` looks like:

```js
const VisibleTopics = (topics,show) => {
    switch(show) {
        case 'new': return getLatest(topics)
        case 'topweek': return getLastWeekPosts(topics, new Date().toISOString())
        case 'topday': return getTodayPosts(topics,new Date().toISOString())
        case 'topall': return sortByVotes(topics,new Date().toISOString())
        default: return TopX(topics,20)
    }
}
```

check this out in the `/components/topics.js` file

Now for the implementations

#### Upvote and downvote

Very simple `.map` implementation:

```js
export const upvote = (arr,id) => arr.map(topic => topic.id === id ? {...topic,votes:++topic.votes} : topic)

export const downvote = (arr,id) => arr.map(topic => topic.id === id ? {...topic,votes:--topic.votes} : topic)
```

#### Top topics for time period

This is done in two parts. First, the date until which the posts will be retreived is calculated using `getYesterday` or `getLastWeek`. That date is then used to filter the topics before it and the result is sorted by votes:

```js
export const getLastWeekPosts = (arr,date) => (
    sortByVotes(arr.filter(topic => topic.timestamp >= getLastWeek(date) && topic.timestamp <= date ))
)
```

#### Latest topics

Very simple `.sort` implementation:

```js
export const getLatest = topics => (
    topics.sort((a,b) => a.timestamp > b.timestamp ? -1 : 1)
)
```

#### Top 20

Uses `sortByVotes` and then slices the array:

```js
export const TopX = (topics,x) => (
    sortByVotes(topics).slice(0,x)
)

export const sortByVotes = topics => (
    topics.sort((a,b) => a.votes > b.votes ? -1 : 1)
)
```


---------------
App by [esaminu](https://github.com/esaminu) / Osman Abdelnasir
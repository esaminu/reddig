import { upvote, downvote } from './upvotesSorts'


export const Reducer = (state = {topics: []}, action) => {
    switch(action.type) {
        case 'ADD_TOPIC': return {...state, topics: state.topics.concat(action.topic)}
        case 'ADD_TEST_DATA': return {topics: action.topicData}
        case 'UPVOTE': return {topics: upvote(state.topics,action.id)}
        case 'DOWNVOTE': return {topics: downvote(state.topics,action.id)}
        default: return state
    }
}

export default Reducer


export const TopX = (topics,x) => (
    sortByVotes(topics).slice(0,x)
)

export const sortByVotes = topics => (
    topics.sort((a,b) => a.votes > b.votes ? -1 : 1)
)

export const getLatest = topics => (
    topics.sort((a,b) => a.timestamp > b.timestamp ? -1 : 1)
)

export const getYesterday = date => {
    var MM = parseInt(date.substring(5,7))
    var DD = parseInt(date.substring(8,10)) 
    if(DD >= 10) {
        return date.substring(0,8) + (parseInt(date.substring(8,10)) - 1) + date.substring(10)
    } else {
        if(DD > 1) {
            return date.substring(0,8) + '0' + (parseInt(date.substring(8,10)) - 1) + date.substring(10)
        } else {
            if(MM > 1) {
               return MM >= 10 ? date.substring(0,5) + (MM - 1) + '-' + daysInMonth(MM - 1,parseInt(date.substring(0,4))) :
               date.substring(0,5) + '0' + (MM - 1) + '-' + daysInMonth(MM - 1,parseInt(date.substring(0,4)))
            } else {
                return (parseInt(date.substring(0,4)) - 1) + "-12-31" + date.substring(10)
            }
        }
    }
}

export const getLastWeek = date => {
    var MM = parseInt(date.substring(5,7))
    var DD = parseInt(date.substring(8,10)) 
    if(DD > 8) {
        return DD >= 10 ? date.substring(0,8) + (parseInt(date.substring(8,10)) - 7) + date.substring(10) :
        date.substring(0,8) + '0' + (parseInt(date.substring(8,10)) - 7) + date.substring(10)
    } else {
        if(MM > 1) {
            return MM>=10 ?  date.substring(0,5) + (MM - 1) + '-' + (daysInMonth(MM,parseInt(date.substring(0,4))) - (7 - DD)) + date.substring(10) :
            date.substring(0,5) + '0' + (MM - 1) + '-' + (daysInMonth(MM,parseInt(date.substring(0,4))) - (7 - DD)) + date.substring(10)
        } else {
            return (parseInt(date.substring(0,4)) - 1) + "-12-" + (31 - (7 - DD)) + date.substring(10)
        }
    }
}

const daysInMonth = (month,year) => {
    return new Date(year, month, 0).getDate();
}

export const getLastWeekPosts = (arr,date) => (
    sortByVotes(arr.filter(topic => topic.timestamp >= getLastWeek(date) && topic.timestamp <= date ))
)

export const getTodayPosts = (arr,date) => (
    sortByVotes(arr.filter(topic => topic.timestamp >= getYesterday(date) && topic.timestamp <= date ))
)

export const upvote = (arr,id) => arr.map(topic => topic.id === id ? {...topic,votes:++topic.votes} : topic)

export const downvote = (arr,id) => arr.map(topic => topic.id === id ? {...topic,votes:--topic.votes} : topic)
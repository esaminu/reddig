import React, {Component} from 'react';
import AddStuff from './components/AddStuff';
import TopicList from './components/topicList'
import {connect} from 'react-redux'
import MockData from './MOCK.json'
import NavBar from './components/NavBar';
import about from './about.md'

class App extends Component {
    constructor(props){
        super(props)
        this.state = {
            width: 0
        }
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }
      
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
      
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth});
    }

    render () {
        return (
            <div style={{display:'flex',flexDirection:'column'}}>
                <NavBar width={this.state.width} history={this.props.history} />
                {this.props.match.path != '/about' ? 
                <div style={{display:'flex',flexDirection:'column'}}>
                    <span style={{fontFamily:'Arial',fontSize:24,alignSelf:'center',margin:10,color:'#e53935'}}>{this.props.title}</span>
                    <div className="appBody">
                        <AddStuff width={this.state.width} addTopic={topic=>this.props.addTopic(topic)} addTest={()=>this.props.addTestData()}/>
                        <TopicList show={this.props.match ? this.props.match.params.show : 'home'}/>
                    </div>
                </div> : <div dangerouslySetInnerHTML={{__html: about}} style={{margin:'2%'}}></div>}
            </div>
        )
    }
}

const mapDispatch = dispatch => ({
    addTopic: topic => dispatch({type:'ADD_TOPIC',topic}),
    addTestData: () => dispatch({type:'ADD_TEST_DATA', topicData:MockData})
})

export default connect(null,mapDispatch)(App)